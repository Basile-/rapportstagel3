import matplotlib.pyplot as plt
import json

from datetime import datetime
from math import log

pathDir = ""


with open(pathDir+"data.json",'r') as file:
    DATA = json.load(file)


plt.close()


## Courbes brutes
dataName="908604632.csv"

with open(pathDir + "autocell.json",'r') as file:
    txt="["+file.read()+"]"
    AUTOCELL = json.loads(txt)

ending=1
Y = [DATA[dataName]["Y"]] + list(map(lambda x:x["Y"], AUTOCELL[-ending:]))
leg = [DATA[dataName]["name"]] + list(map(lambda x:f"Courbe autoCell\n pCCS={x['pC_ContaminatingS']}; pS={x['pSourceContamination']};\n TTL={x['TimeToLive']}; pNLA={x['pNeverListenAgain']}\n pFCS={x['pFanContaminatingS']}", AUTOCELL[-ending:]))

XY=[]
extremums=[]
for y in Y:
    extremums.append(max(y))

for x,y in XY:
    extremums.append(max(y))
if extremums!=[]:
    max_E=max(extremums)


for k in range(len(Y)):
    YT=[]
    XT=[]
    sum=0
    cor=max_E/extremums[k]
    print(cor)
    for i in range(len(Y[k])):
        YT.append(Y[k][i]*cor)
        XT.append(i)
    plt.plot(XT,YT)

for x,y in XY:
    plt.plot(x,list(map(lambda x:max_E*x/extremums[k], Y[k])))

plt.legend(leg)
plt.title("Loco Contigo - DJ Snake")
plt.show()
#plt.savefig(pathDir + "Images/AutoCellData_Loco.svg")

## Courbes journalières = 1 courbe par jour de la semaine
allY=[]
for Y in allY:
    YT=[[],[],[],[],[],[],[]]
    XT=[[],[],[],[],[],[],[]]
    for i in range(len(Y)):
        YT[i%7].append(Y[i])
        XT[i%7].append(i)
    for i in range(7):
        plt.plot(XT[i],YT[i])


##Point hebdomadiare

with open(pathDir + "autocell.json",'r') as file:
    txt="["+file.read()+"]"
    AUTOCELL = json.loads(txt)

leg=["Data Toosie Slide"]
ending=1
Y = [DATA[dataName]["Y"]] + list(map(lambda x:x["Y"], AUTOCELL[-ending:]))
leg = [DATA[dataName]["name"]] + list(map(lambda x:f"pCCS={x['pC_ContaminatingS']}; pS={x['pSourceContamination']}; pNLA={x['pNeverListenAgain']}", AUTOCELL[-ending:]))

XY=[]
extremums=[]
for y in Y:
    extremums.append(max(y))

for x,y in XY:
    extremums.append(max(y))
if extremums!=[]:
    max_E=max(extremums)


for k in range(len(Y)):
    YT=[]
    XT=[]
    sum=0
    c=0
    cor=max_E/extremums[k]
    print(cor)
    for i in range(len(Y[k])):
        sum+=Y[k][i]*cor
        c+=1
        if i%7==6:
            YT.append(sum/c)
            XT.append(i)
            sum=0
            c=0
    if c!=0:
        YT.append(sum/c)
        XT.append(i)
    plt.plot(XT,YT)

for x,y in XY:
    plt.plot(x,list(map(lambda x:max_E*x/extremums[k], Y[k])))

plt.legend(leg)
plt.xlabel("temps")
plt.ylabel("nb_ecoutes")
plt.show()
#plt.savefig(pathDir + "Images/AutoCellData_BYG.svg")

##
plt.legend(leg)

d=datetime.now()
if input("Is the name defined")=="Y":
    name=dataName+"_Data"
    nameF = f"Images/{name}_{d.year}-{d.month}-{d.day}_{d.hour}-{d.minute}-{d.second}.png"
    plt.savefig(pathDir+nameF)

plt.show()
