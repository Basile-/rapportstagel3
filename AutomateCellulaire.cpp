#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
using namespace std;

#define nNode 100
int nTours = 350;

// probabilities for S
float pC_ContaminatingS = 0.01; 
float pSourceContamination = 0.01; 
float pFanContaminatingS = 0.05;

// probabilities for C
float TimeToLive = 20;
float pBecomingFan = 0.1;


// probabilities for G
float pNeverListenAgain = 0.004;
float pC_ContaminatingG = 0.02;
float pFanContaminatingG = 0.1;

// probabilities for F
float pFanStopListenning = 0.00035;





float a = 0.85; //Constant used to create Barabasi-Albert graphs
float densite=0.2; //Constant used to create random graphs
char mode = 'M'; //Mode de génération de graphe, avec l'initiale : Aleatoire ou Barabasi-albert ou Matrice ou Text_to_graph


struct Node{
    char state; //the one we will base on during the calculation
    char nextState; //the one we modify during the calculation
    vector <int> in; //vertex that could infect the Node
    int age; // a normal listener will listen 10 times
};


struct Node graph[nNode];
int used[nNode];

inline double prob();
int randint(int min, int max);
int not_all_used();
int GenerateRandomGraph(float density);
int GenerateBarabasiGraph();
int GenerateMatrixGraph();

int countTypeNeighbour(char Mode, int i);
int step();

int printGraph();
int saveGraph();
int textToGraph();

int saveFig(vector <int>);





int main() {
    if (mode=='A') GenerateRandomGraph(densite);
    else if (mode=='B') {
        for(int i=0; i<nNode; i++) {used[i]=0;}
        GenerateBarabasiGraph();
    }
    else if (mode=='T') textToGraph();
    else if (mode=='M') {
        if (not GenerateMatrixGraph()) return 0;
    }
    else {
        printf("Error : mode forbiden");
        return 0;
    }
    //saveGraph(); //to save graph in order to reuse it with TextToGraph mode
    //printGraph();
    
    vector <int> Y;

    graph[0].state='C';
    for(int i=0; i<nTours; i++) Y.push_back(step());
    cout << '['; //print the evolution of the automata
    for(int i=0; i<nTours; i++) cout << Y[i] << ", ";
    cout << ']' << endl;
    saveFig(Y);
    return 0;
}






inline double prob(void) {
  random_device rd; // used as random seed
  static mt19937 rng; // create random generator
  rng.seed(rd()); // input the seed into the rng
  uniform_real_distribution<double> probability(0, 1); // create distribution
  return probability(rng); // return the random probability
}

int randint(int min, int max){
    random_device rd;     // only used once to initialise (seed) engine
    mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    uniform_int_distribution<int> uni(min,max); // guaranteed unbiased
    return uni(rng);
}

int not_all_used(){
    for(int i=0; i<nNode; i++) {if (used[i]==0) return 1;}
    return 0;
}

int countTypeNeighbour(char State, int i){ //count how many of nodes of 'State' are in the neighbourhood of i
    int sum=0;
    for(int j=0; j<graph[i].in.size(); j++){
        if(graph[graph[i].in[j]].state==State) {
            sum++;
        }
    }
    return sum;
}




int step(){
    for(int i=0; i<nNode; i++){
        float P = prob();
        if (graph[i].state=='S' && (P<pSourceContamination || P<pC_ContaminatingS*countTypeNeighbour('C', i) || P<pFanContaminatingS*countTypeNeighbour('F', i))){
            graph[i].nextState='C';
            graph[i].age++;
            if (P<pSourceContamination) so++; // Detailed Stats : source contamination
            else tr++; // Detailed Stats : transmission

        }
        else if (graph[i].state=='C'){
            if (P<graph[i].age/TimeToLive){
                graph[i].nextState='G';
                graph[i].age=0;
                ag++;
            }
            else if (P<pBecomingFan){
                graph[i].nextState='F';
                graph[i].age++;
            }
            else {
                graph[i].nextState='C';
                graph[i].age++;
            }
        }
        else if (graph[i].state=='G' && P<pNeverListenAgain){ //Someone who don't listen won't listen anymore
            graph[i].nextState='I';
        }
        else if (graph[i].state=='G' && P<max(pC_ContaminatingG, pFanContaminatingG)){ //Someone who don't listen won't listen anymore
            graph[i].nextState='C';
            graph[i].age++;
            tr++;
        }
        else if (graph[i].state=='F' && P < pFanStopListenning){ //Fan
            graph[i].nextState='G';
        }
        else { //Keep S, G, F or I when they don't change; (C must age)
            graph[i].nextState=graph[i].state;
        }
    }
    int sum=0;
    for(int i=0; i<nNode; i++){
        graph[i].state = graph[i].nextState; //update the graph
        if (graph[i].state == 'C' || graph[i].state == 'F') sum++; //count how many people listen to the music
    }
    return sum;
}








int GenerateBarabasiGraph(){
    int k,pk,l,pl,nbE=0; //nb Edge
    while (not_all_used()){
        do { k=randint(0,nNode-1);
        }while(used[k]);
        do{ l=randint(0,nNode-1);
        }while(nbE && (k==l || pow(float(graph[l].in.size())/nbE, a)<prob())); //sort si nbE==0 OU k!=l ET P>Random
        graph[k].in.push_back(l);
        graph[l].in.push_back(k);
        nbE+=2;
        used[k]++;
        pk=k;
        pl=l;
        graph[k].state='S';
        graph[k].nextState='S';
    }
    return 1;
}

int GenerateRandomGraph(float density) {
    for(int i = 0; i < nNode; i++) {
        graph[i].state='S';
        graph[i].nextState='S';
        for(int j = 0; j < nNode; j++) {
            if(i != j && prob() < density) {
                graph[i].in.push_back(j);
            }
        }
    }
    return 0;
}

int GenerateMatrixGraph() {
    double M=sqrt(nNode);
    if (M!=floor(M)){
        printf("Error : mode Matrix activated but nNode isn't a square\n");
        return 0;
    }
    int m = int(M);
    for (int i=0; i<nNode;i++){
        graph[i].state='S';
        graph[i].nextState='S';
        graph[i].in.push_back((i+(m-1)*m)%(m*m));
        graph[i].in.push_back((i+m)%(m*m));
        graph[i].in.push_back((i-1+m)%m+(i/m)*m);
        graph[i].in.push_back((i+1)%m+(i/m)*m);
    }
    return 1;
}

int textToGraph(){
    int i=0,j,n,m;
    cin>>m;
    if (m!=nNode) {
        throw invalid_argument("Error : nNode != m, I can't initialize the graph\n");
    }
    while(cin>>n){
        for(int k=0; k<n; k++){
            cin >> j;
            graph[i].in.push_back(j);
            graph[i].state='S';
            graph[i].nextState='S';
        }
        i++;
    }
    return 1;
}









int printGraph(){
    int sum=0;
    for (int i=0; i<nNode; i++){
        cout << i << " -> [ ";
        for (int j=0; j<graph[i].in.size(); j++){
            cout << graph[i].in[j] << " ";
        }
        cout << ']' << endl;
        sum+=graph[i].in.size();
    }
    cout << endl << "|V| = " << sum << endl;
    return 1;
}
int saveGraph(){
    fstream my_file;
	my_file.open("out", ios::out);
	if (!my_file) cout << "File not created!"<<endl;
	else {
        my_file << nNode << endl;
        for (int i=0; i<nNode; i++){
            my_file << graph[i].in.size() << " ";
            for (int j=0; j<graph[i].in.size(); j++){
                my_file << graph[i].in[j] << " ";
            }
            my_file << endl;
        }
        cout << "Saving successfull!"<<endl;
	}
    return 1;
}









int saveFig(vector <int> Y){
    fstream my_file;
	my_file.open("autocell.json", ios::app);
	if (!my_file) cout << "File not created!"<<endl;
	else {
        my_file <<",\n{\n\"mode\" : \""<<mode<<"\",\n\"densite\":"<<densite<< ",\n\"pC_ContaminatingS\" : "<<pC_ContaminatingS<<
        ",\n\"pFanContaminatingS\" : "<<pFanContaminatingS<<",\n\"pSourceContamination\" : "<<pSourceContamination<<
        ",\n\"TimeToLive\" : "<<TimeToLive<<",\n\"pBecomingFan\" : "<<pBecomingFan<<",\n\"pNeverListenAgain\" : "<<pNeverListenAgain<<
        ",\n\"pC_ContaminatingG\" : "<<pC_ContaminatingG<<",\n\"pFanContaminatingG\" : "<<pFanContaminatingG<<
        ",\n\"pFanStopListenning\" : "<<pFanStopListenning<<",\n\"Y\":[";
        for(int i=0;i<nTours-1;i++){my_file<<Y[i]<<", ";}
        my_file<<Y[nTours-1]<<"]\n}\n";
		my_file.close(); 
        cout << "Saving successfull!"<<endl;
	}
	return 0;
}

